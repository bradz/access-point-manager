package network;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Vector;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

public class Util {
	
	public static boolean lookupNever(String id){
		try {
			String[] cmd = new String[]{"/bin/sh", "-c", "nmcli -f name,timestamp-real con"};
			Process proc = Runtime.getRuntime().exec(cmd);
			String line = null;
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			while ((line = procReader.readLine()) != null) {
				if (line.startsWith(id)) {
					if (line.contains("never"))	
						return true;
				}
			}
		} catch (Exception e) {
			printlog("lookupNever("+id+"): exception: " + e.getLocalizedMessage());
		}

		return false;
	}
	
	public static void testNewConnection(String fu){
 		printlog("monitorThread(): test profile if exists: "+ fu);
		if (fu == null) return;
	 	
		new Thread(new Runnable() {
			@Override
			public void run() {
		
				try {Thread.sleep(45000);} catch (InterruptedException e) {}
					
		 		printlog("monitorThread(): test profile if exists: "+ fu);
			
		 		if(Util.lookupNever(fu)) { 
		 			printlog("monitorThread(): never seen, deleting: "+ fu);		
		 			Util.connectionDelete(fu);
		 		}
			}
		}).start();
	}

	static void disconnect(){
		
		try {
			String id = WirelessConnection.getCurrentConnectionID();
			printlog("disconnect(): from id: " + id);
			if (id == null) WirelessConnection.lookupWirelessConnections();
			if (id == null) {
				 printlog("disconnect(): was not connected, toggle");
				 Util.wifiDisable();
				 Thread.sleep(2000);
				 Util.wifiEnable();
				 return;
		 }
		
			Runtime.getRuntime().exec(new String[]{"/bin/sh", "-c", "nmcli c down id \""+id+"\""}).waitFor();
		} catch (Exception e) {
			printlog("disconnect(): exception: " + e.getLocalizedMessage());
		}
		// NetworkServlet.lookupCurrentSSID();
		WirelessConnection.lookupWirelessConnections();
	}
	
	static void connectionDelete(final String ssid) {
		printlog("Util.connectionDelete(): deleting: " + ssid);
		try {
			Runtime.getRuntime().exec(new String[]{"/bin/sh", "-c", "nmcli con delete id \"" + ssid+"\""}).waitFor();
		} catch (Exception e) {
			printlog("connectionDelete(): exception: " + e.getLocalizedMessage());
		}
		WirelessConnection.lookupWirelessConnections();
		lookupCurrentSSID(NetworkServlet.wdev);
		printlog("connectionDelete(): current ssid: " + NetworkServlet.currentSSID);

	}

	public static int countRadios() {
		int radios = 0;
		try {
			String line = null;
			Process proc = Runtime.getRuntime().exec(new String[]{"/bin/sh", "-c", "nmcli dev"});
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			while ((line = procReader.readLine()) != null) {
				
				// UBUNTU MATE has a 'wifi-p2p' interface 
				if(((line.contains("wireless") || line.contains("wifi")) && !line.contains("wifi-p2p"))){  
					
					printdebug("countRadios() wdev: "+ line.split(" +")[0]);
					radios++;		
				}
			}
		} catch (Exception e) {
			printlog("countRadios(): exception: " + e.getLocalizedMessage());
		}
		
		printlog("countRadios(): found: " + radios);
		return radios;
	}

	// use specific radio 
	public static String ping(String ip, String wdev) {
		String line = null;				
        Process proc = null;
		BufferedReader procReader = null;
		try{	

			// 500ms is a fail 
			proc = Runtime.getRuntime().exec(new String[]{"ping", "-c1", "-I"+wdev, "-i0.5", ip});
			procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			while ((line = procReader.readLine()) != null) {				
				if (line.contains("time=")) {	
					line = line.split("time=")[1];
					break;
				}
				
				if (line.contains("100% packet loss")) line = null;
			}	
		} catch (Exception e) {
			printlog("monitorThread: error reading from proc: " + e.getLocalizedMessage());
		}	
		return line;
	}
	
	public static boolean wifiRadioReady(String wdev){
		try {
			Process proc = Runtime.getRuntime().exec(new String[]{"nmcli", "d" }); 
			String line = null;
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			while ((line = procReader.readLine()) != null) {
				if (line.contains(wdev)){
					if (line.contains("unavailable")) {
						printlog("wifiRadioReady(): unavailable: " + wdev);
						return false;
					}
				}
			}	
		} catch (Exception e) {
			printlog("wifiRadioReady(): exception: " + e.getLocalizedMessage());
		}
		
		return true;
	}
	
	public static void setAutoConnect(String name, boolean auto) {
		try {
			if (auto){
				Runtime.getRuntime().exec(
						new String[]{"/bin/sh", "-c", "nmcli c modify \""+name+"\" connection.autoconnect yes" }).waitFor();
			} else {	
				Runtime.getRuntime().exec(
					new String[]{"/bin/sh", "-c", "nmcli c modify \""+name+"\" connection.autoconnect no" }).waitFor();		
			}
		} catch (Exception e) {e.printStackTrace();}
		WirelessConnection.lookupWirelessConnections(); 		
	}
	
	public static void setPriority(String name, int i) {
		if (i < 0) return;
		printdebug("setPriority(): "+name+" "+i);
		try {
			
			Runtime.getRuntime().exec(
				new String[]{"/bin/sh", "-c", "nmcli c modify \""+name+"\" connection.autoconnect-priority "+i }).waitFor();

		} catch (Exception e) {e.printStackTrace();}
		WirelessConnection.lookupWirelessConnections(); 		
	}
	
	//  TODO: arp on wdev iface only?
	// bradzcave                ether   c4:41:1e:5c:98:4c   C                     wlx1cbfce17a9fe
	public static Vector<String> lookupARP(String wdev) { 
		Vector<String> users = new Vector<String>();
		Process proc = null;
		BufferedReader procReader = null;
		try{	
			String line = null;		
			proc = Runtime.getRuntime().exec(new String[]{"arp", "-i", wdev});
			procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			while ((line = procReader.readLine()) != null) 
				if( ! line.startsWith("Address") 
						&& !line.contains("incomplete")
						&& !line.contains("no match found")) {
					String ip = line.split(" +")[0];
					if ( !users.contains(ip)) 
						users.add(ip);
				}
		} catch (Exception e) {
			printlog("arpThread: error reading from proc: " + e.getLocalizedMessage());
		}	
//		try {
//			procReader.close();
//		} catch (IOException e) {
//			printlog("arpThread: error closing proc: " + e.getLocalizedMessage());
//		} 
		return users;
	}
	
	 // choose first wifi device. force wdev name in settings. must be supplied if multiple radios
	static String lookupDevice() {
		
		if (NetworkServlet.configured_wdev) {
			printdebug("lookupDevice() configured wdev: "+NetworkServlet.wdev + ", skipping..");
			return NetworkServlet.wdev;	
		}
		
		try {
			String line = null;
			Process proc = Runtime.getRuntime().exec(new String[]{"/bin/sh", "-c", "nmcli dev"});
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			while ((line = procReader.readLine()) != null) {
				
				// UBUNTU MATE has a 'wifi-p2p' interface 
				if(((line.contains("wireless") || line.contains("wifi")) && !line.contains("wifi-p2p"))){  
					String[] list = line.split(" +");
					return list[0];
				//	printdebug("lookupDevice() wdev: "+wdev);
				}
			}
		} catch (Exception e) {
			printlog("lookupDevice(): no devices connected");
		}
		
		return null;
	}

	public static String lookupCurrentSSID(String wdev) { 
		BufferedReader procReader = null;
		try{	
			String line = null;		
			//Process proc = Runtime.getRuntime().exec(new String[]{"/bin/sh", "-c", "nmcli | grep " + wdev});
		
			Process proc = Runtime.getRuntime().exec(new String[]{"/bin/sh", "-c", "nmcli -t -f active,ssid,device dev wifi | grep yes"});

			procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			while ((line = procReader.readLine()) != null) {
				
				//if(line.contains("connected") && !line.contains("disconnected")) {
					
					// whitesace
					
					//nmcli -t -f active,ssid,device dev wifi | grep yes
					//yes:bd:wlan0
					// String g = line.substring(line.indexOf("connected to")+12, line.length());
					// 
					// return g;
					
				//	if (line.contains(wdev)) {
						String[] tok = line.split(":");
						printlog("Util.lookupCurrentSSID(): " + tok[1]);
						return tok[1];
			//		}
					
					
			
			}
		} catch (Exception e) {
			printlog("Util.lookupCurrentSSID(): " + e.getLocalizedMessage());
		}	
		return null;
	}
	
	public static boolean isHidden(String id) {
		try {	
			String[] cmd = new String[]{"/bin/sh", "-c", "nmcli -t c show id \"" +  id + "\" | grep wireless.hidden:"};
			Process proc = Runtime.getRuntime().exec(cmd);
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			String line = procReader.readLine();
			if (line.contains(":yes")) return true;
		} catch (Exception e) {}//e.printStackTrace();}
		return false;
	}
	
	public static boolean setHidden(String id, boolean flag) {
		try {	
			String var = "no";
			if (flag) var = "yes";
			String[] cmd = new String[]{"/bin/sh", "-c", "nmcli -t c modify \"" +  id + "\" 802-11-wireless.hidden "+ var};
			Process proc = Runtime.getRuntime().exec(cmd);
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			String line = procReader.readLine();
			if (line.contains(":yes")) return true;
		} catch (Exception e) {}//e.printStackTrace();}
		return false;
	}
	
	public static String getSSID(String id) {
		try {	
			String[] cmd = new String[]{"/bin/sh", "-c", "nmcli -t c show id \"" +  id + "\" | grep wireless.ssid:"};
			Process proc = Runtime.getRuntime().exec(cmd);
			proc.waitFor();
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			String line = procReader.readLine();
			if (line.toLowerCase().contains("error")) return null;
			String[] args = line.split(":");
			return args[1].trim();
		} catch (Exception e) {e.printStackTrace();}
		return null;
	}
	
	public static String getSSIDbyUUID(String uuid) {	
		try {		
			String[] cmd = new String[]{"/bin/sh", "-c", "nmcli -t c show uuid " +  uuid + " | grep wireless.ssid:"};
			Process proc = Runtime.getRuntime().exec(cmd);
			proc.waitFor();
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			String line = procReader.readLine();
			if (line.toLowerCase().contains("error")) return null;
			String[] args = line.split(":");
			return args[1].trim();
		} catch (Exception e) {e.printStackTrace();}
		return null;
	}

	static boolean getModeUUID(final String uuid) {
		try {		
			String[] cmd = new String[]{"/bin/sh", "-c", "nmcli -t c show uuid " + uuid + " | grep wireless.mode"};
			Process proc = Runtime.getRuntime().exec(cmd);
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			String line = procReader.readLine();	
			if (line.toLowerCase().contains("error")) return true;

			if (line.split(":")[1].equals("Hotspot")) return true;
			if (line.split(":")[1].equals("ap")) return true;
			
		} catch (Exception e) {e.printStackTrace();}	
		return false;
	}
	
	public static void createAPConnection(final String name, String wdev){
		
		if (wdev == null) {
			printdebug("createAPConnection(): wdev is null, skipping.. ");
			return;
		}
		
		printlog("createAPConnection("+name+"): creating open ap..");

		try {
			
			// create a uuid
			Runtime.getRuntime().exec(
				new String[]{"/bin/sh", "-c", "nmcli c add type wifi ifname "+ wdev +" con-name \""+name+ "\" autoconnect no ssid \""+name+"\""}).waitFor();

			// set as AP mode, no password
			Runtime.getRuntime().exec(
				new String[]{"/bin/sh", "-c", "nmcli c modify \""+name+"\" 802-11-wireless.mode ap ipv4.method shared" }).waitFor();

		} catch (Exception e) {e.printStackTrace();}
		WirelessConnection.lookupWirelessConnections();
	}

	public static void createHotspotConnection(String ssid, String password, String dev){
 		
		if (ssid == null || dev == null) { 
			printdebug("createHotspotConnection(): no args.. ");
			return;
		}

		printdebug("createHotspotConnection(): ssid: "+ssid+", " + password + ", " + dev);
	
		if (password == null) {
			
			printdebug("createHotspotConnection(): null password, create ap connection..");
			createAPConnection(ssid, dev);
			return;

		} else {
			try {	
				Runtime.getRuntime().exec(
					new String[]{"/bin/sh", "-c", "nmcli d wifi hotspot ifname " + dev + " ssid \"" + ssid + "\" password \"" + password +"\""}).waitFor();
			} catch (Exception e) {
				printlog("createHotspotConnection(): exception: " + e.getLocalizedMessage());
			}
		}
		WirelessConnection.lookupWirelessConnections();
	}
	
	public static String linuxVersion(){
		try {
			String[] cmd = new String[]{"/bin/sh", "-c", "lsb_release -a | grep Description:"};
			Process proc = Runtime.getRuntime().exec(cmd);
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			return procReader.readLine().split(":")[1].trim(); 
		} catch (Exception e) {e.printStackTrace();}
		return null;
	}
	
	public static String linuxCodeName(){
		try {
			String[] cmd = new String[]{"/bin/sh", "-c", "lsb_release -sc"};
			Process proc = Runtime.getRuntime().exec(cmd);
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			return procReader.readLine();
		} catch (Exception e) {e.printStackTrace();}
		return null;
	}
	
	public static String nmcliVersion(){
		try {
			String[] cmd = new String[]{"/bin/sh", "-c", "nmcli -v"};
			Process proc = Runtime.getRuntime().exec(cmd);
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			return procReader.readLine(); 
		} catch (Exception e) {e.printStackTrace();}
		return null;
	}
	
	public static String sharkVersion(){
		try {
			String[] cmd = new String[]{"/bin/sh", "-c", "tshark -v"};
			Process proc = Runtime.getRuntime().exec(cmd);
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			return procReader.readLine(); 
		} catch (Exception e) {e.printStackTrace();}
		return null;
	}
	
	public static String getHostName(){
		try {
			String[] cmd = new String[]{"/bin/sh", "-c", "hostname"};
			Process proc = Runtime.getRuntime().exec(cmd);
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			return procReader.readLine();
		} catch (Exception e) {e.printStackTrace();}
		return null;
	}
	
	public static String getEthernetDevice() {
		try {
			Process proc = Runtime.getRuntime().exec(new String[]{"/bin/sh", "-c", "nmcli dev"});
			String line = null;
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			while ((line = procReader.readLine()) != null) {
				if (line.contains("ethernet")){
					String[] list = line.split(" +");
					return list[0];
				}
			}
		} catch (Exception e) {
			printlog("lookupEthernetDevice(): exception: " + e.getLocalizedMessage());
		}
		return null;
	}
	
	
	public static String XMLtoString(Document doc) {
		String output = null;
		try {
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(doc), new StreamResult(writer));
			output = writer.getBuffer().toString().replaceAll("\n|\r", "");
		}
		catch (Exception e) {}
		return output;
	}
	
	public static void killJetty(){
		try {
			Runtime.getRuntime().exec(new String[]{"/bin/sh", "-c", "java -jar stop.jar"});
		} catch (Exception e) {}
	}
	
	public static void killApplet(){
		try {
			Runtime.getRuntime().exec(new String[]{"/bin/sh", "-c", "pkill nm-applet"});
		} catch (Exception e) {}
	}
	
	public static void reboot(){
		try {
			Runtime.getRuntime().exec(new String[]{"/bin/sh", "-c", "reboot now"});
		} catch (Exception e) {e.printStackTrace();}
	}


	public static void wifiEnable(){
		try {
			Runtime.getRuntime().exec(new String[]{"nmcli", "radio", "wifi", "on" }).waitFor();
		} catch (Exception e) {
			printlog("wifiEnable(): exception: " + e.getLocalizedMessage());
		}
	}
	
	public static void wifiDisable(){
		try {
			Runtime.getRuntime().exec(new String[]{"nmcli", "radio", "wifi", "off" }).waitFor();
		} catch (Exception e) {
			printlog("wifiEnable(): exception: " + e.getLocalizedMessage());
		}
	}
	
	public static String formatFloat(double number, int precision) {

		String text = Double.toString(number);
		if (precision >= text.length()) {
			return text;
		}

		int start = text.indexOf(".") + 1;
		if (start == 0) return text;
		if (precision == 0) return text.substring(0, start - 1);
		if (start <= 0) return text;
		else if ((start + precision) <= text.length())
			return text.substring(0, (start + precision));
		else return text;
	}


	public static HashMap<String, String> importFile(){
		HashMap<String, String> settings = new HashMap<String, String>();
		try {
			String line; // allow whitespace, ssid etc 
			FileInputStream filein = new FileInputStream("settings.txt");
			BufferedReader reader = new BufferedReader(new InputStreamReader(filein));
			while ((line = reader.readLine()) != null) {
				line = line.trim();
				if (line.length() == 0) continue;
				if (!line.contains(" ")) continue;
				String arg = line.substring(0, line.indexOf(" ")).trim();
				String rest = line.substring(line.indexOf(" "), line.length()).trim();
				settings.put(arg, rest);
				printdebug("importFile(): " + arg + " " + rest);
			}
			reader.close();
			filein.close();
		} catch (Exception e) {				
			printdebug("importFile(): " + e.getLocalizedMessage());
		}
		return settings;
	}

	
	public static boolean validIP(String ip) {
		try {

			if (ip == null || ip.isEmpty()) return false;
			String[] parts = ip.split( "\\." );
			if ( parts.length != 4 ) return false;
			for ( String s : parts ) {
				int i = Integer.parseInt( s );
				if ( (i < 0) || (i > 255) )
					return false;
			}

			if(ip.endsWith(".")) return false;

			return true;
		} catch (NumberFormatException nfe) {
			return false;
		}
	}
	/*
	
	void lookupEthernetUUID(){
		try {
			String[] cmd = new String[]{"/bin/sh", "-c", "nmcli -t -f type,uuid c | grep ethernet"}; 
			Process proc = Runtime.getRuntime().exec(cmd);
			String line = null;
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			while ((line = procReader.readLine()) != null) {
				ethUUID = line.split(":")[1]; 
			}
		} catch (Exception e) {
			printlog("lookupEthernet(): exception: " + e.getLocalizedMessage());
		}
	}
	


	public static String formatTime(String milliSeconds){
      	
		String dateFormat = "dd-MM-yyyy hh:mm";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(Long.parseLong(milliSeconds));
		return simpleDateFormat.format(calendar.getTime());
	}	
	 */
	
	
	public static String formatTime(String milliSeconds){
      	
		String dateFormat = "dd-MM-yyyy hh:mm";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(Long.parseLong(milliSeconds));
		return simpleDateFormat.format(calendar.getTime());
	}	

	static String getDateStamp() {
		// Calendar cal = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("hh:mm");
		String date = dateFormat.format(new Date());
		return (date);
	}
	
	public static void printlog(String string) {
		NetworkServlet.printdebug(string);
	}

	public static void printdebug(String string) {
		NetworkServlet.printdebug(string);
	}

	static void toFile(String logfilename, final String str){
		
		// if in settings, create log file
		if(logfilename == null) return;
		
		RandomAccessFile logfile = null;
			
		if ( ! new File(logfilename).exists()) {
			try {
				if( ! new File(logfilename).createNewFile()) {
					printlog("can't create file: " + logfilename);
					return;
				}
			} catch (IOException e1) {
				printlog("can't create file: " + e1.getLocalizedMessage());
				return;
			}
		}
		
		if (logfile == null) {
			try {
				logfile = new RandomAccessFile(new File(logfilename), "rw");
			} catch (Exception e) {
				printlog(e.getLocalizedMessage());
				return;
			}
		}
		
		try {
			logfile.seek(logfile.length());
			logfile.writeBytes(new Date().toString() + ", " + str + "\r\n");
		} catch (Exception e) {
			printlog(e.getLocalizedMessage());
		}
		
		try {
			logfile.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
