package network;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

public class AccessPoint {

	public static Vector<AccessPoint> access = new Vector<AccessPoint>();
	public static int hidden = 0;
	
	Boolean pwrequied = null;
	int signalstrength = 0;
	String ssid = null;
	String bssid = null;
	String freq = null;
	
	AccessPoint(final String[] tok){
		
		//Util.printdebug("AccessPoint(): ssid: "+ tok[1]);
		//Util.printdebug("getAccessPoints(): freq: "+ tok[11]);
		//Util.printdebug("getAccessPoints(): sig:  "+ tok[13]);
		//printdebug("getAccessPoints(): hex:  "+ hex);
		//printdebug("getAccessPoints(): pw:   "+ tok[15]);
		
		String hex = "";
		for( int i = 3 ; i < 9; i++) hex += tok[i].replace("\\", ":");
		bssid = hex;
		ssid = tok[1];
		freq = tok[11];
		signalstrength = Integer.parseInt(tok[13]);
		if (tok[15].trim().length() == 0) pwrequied = Boolean.FALSE;
		else pwrequied = Boolean.TRUE;
	}
	
	public String toString() {
		return ssid;
	}
	
	public static synchronized void lookupAccessPoints() {
		
		if (WirelessConnection.isHotspotMode()) return;
			
		hidden = 0;
		Vector<AccessPoint> scan = new Vector<AccessPoint>();
		try {

			String[] cmd = new String[]{"/bin/sh", "-c", "nmcli -t -f ALL dev wifi"};
			Process proc = Runtime.getRuntime().exec(cmd);
			String line = null;
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));

			while ((line = procReader.readLine()) != null) {
				
				// restrict to only this wdev?
				if ( ! line.contains(NetworkServlet.wdev))
					Util.printlog("lookupAccessPoints(): warn, not wdev: " + line);

				
				if (line.startsWith("AP[")) {
					final String[] tok = line.split(":");
					if (tok[1].length() > 0) { 
						
						// visible ssid 
						scan.add(new AccessPoint(tok));

					} else hidden++;
				}
			}
		} catch (Exception e) {
			Util.printlog("lookupAccessPoints(): exception: " + e.getLocalizedMessage());
		}
		
		if (scan.size() == 0) Util.printlog(("scan failed, used last size: " + access.toString()));

		if (scan.size() > 0) {
			
			Util.printdebug(("scan results: " + scan.size() + " hidden ssid: " + hidden));
			access = scan;
			sort(access);
			// Util.printlog(("scan reults: " + access.toString()));

		}
	}
	
	public static void sort(Vector<AccessPoint> list) {
		Collections.sort(list, new Comparator<AccessPoint>() {
			public int compare(AccessPoint f1, AccessPoint f2) {	
				
				String a = f1.ssid;
				String b = f2.ssid;
				return b.compareTo(a);
						
			}
		});
	}
	
	static boolean dup(final String ssid) {	
		int j = 0;
		for (int i = 0 ; i < access.size() ; i++) {
			if (access.get(i).ssid.equals(ssid)) {
				j++;
				//if (j >= 1) {		
				//	Util.printlog(ssid + " is dup  " + ssid + " " + j);
				//	return true;
				//}
			}
		}
		if (j > 1) {		
			// Util.printlog(ssid + " " + ssid + " " + j);
			return true;
		}
		return false;
	}

	public static int indexOf(String ssid) {

//		Util.printlog(ssid + " size of:  " + access.size());
//		Util.printlog(ssid + " index of: " + access);

		for (int i = 0 ; i < access.size() ; i++)
			if (access.get(i).ssid != null)
				if (access.get(i).ssid.equals(ssid))
					return i;

		return -1;
	}
	
	public static String getKnownHTML (String currentSSID) {		
		Vector<AccessPoint> access = AccessPoint.access;
		try {
			StringBuffer known = new StringBuffer();
			for (int i = 0 ; i < access.size() ; i++) {
				if (access.get(i).ssid != null) {
					if (WirelessConnection.isKnownSSID(access.get(i).ssid)) {

						if (currentSSID != null) {
							if (access.get(i).ssid.equals(currentSSID)) continue;
							
//							if (access.get(i).bssid.equals(access.get(indexOf(currentSSID)).bssid )) continue;
						}

						known.append("&nbsp;<a href=\"?action=up&amp;router="
							+ access.get(i) + "\">" + access.get(i).ssid + "</a>&nbsp;&nbsp;"
							+ "&nbsp;&nbsp;<span style='font-size: 12px'> " 
							+ " " + access.get(i).signalstrength + "% &nbsp;&nbsp;" + access.get(i).freq
							+ "</span><br> \n");
						
					}
				}
			}
			
			if (known.length() > 0) {
				known.append("\n<br>\n");
				known.insert(0, "Known connections in range:<br>"+NetworkServlet.gap());
				return known.toString();
			}
			
			// Util.printdebug("getKnownHTML(): accesspoints: "+ access.size());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	
	public static String toHTML(String currentSSID) {		
		Vector<AccessPoint> access = AccessPoint.access;
		try {
			StringBuffer other = new StringBuffer();
			for (int i = 0 ; i < access.size() ; i++) {

				if (currentSSID != null) if (access.get(i).ssid.equals(currentSSID)) continue;
				if (WirelessConnection.isKnownSSID(access.get(i).ssid)) continue;
				
				if (access.get(i).pwrequied ) {
					
					if (dup(access.get(i).ssid)) // use hex, several matching ssids
						other.append("&nbsp;<a href=\"?action=connect&amp;router="
							+ AccessPoint.access.get(i).bssid + "\">" + access.get(i) + "</a>");
					else	
						other.append("&nbsp;<a href=\"?action=connect&amp;router="
							+ AccessPoint.access.get(i) + "\">" + access.get(i) + "</a>");
				} else {
					other.append("&nbsp;<a href=\"?action=open&amp;router="
						+ access.get(i) + "\">" + access.get(i) + "</a>&nbsp;<b>unlocked</b>&nbsp;");		
				}
				
				//other.append("&nbsp;<span style='font-size: 12px'>" + access.get(i).bssid + "</span>&nbsp;");

				other.append("&nbsp;<span style='font-size: 12px'>" + access.get(i).signalstrength + "%&nbsp;");
				other.append("&nbsp;"+ access.get(i).freq + "&nbsp;</span>");
				other.append("<br> \n");
					
			}	
			
			if (other.length() > 0) {
				other.insert(0, "Connections in range:<br>"+NetworkServlet.gap());
				other.append("<br> \n");
				return other.toString();
			}
		} catch (Exception e1) {}		
		return "";
	}
}
