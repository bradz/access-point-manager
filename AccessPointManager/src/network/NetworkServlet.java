package network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


//
// https://developer-old.gnome.org/NetworkManager/stable/nmcli.html
// https://developer-old.gnome.org/NetworkManager/1.30/NetworkManager.html
//
// use tshark to grab host names while in ap mode 

public class NetworkServlet extends HttpServlet {

	static final double VERSION = 1.511;
	static final long serialVersionUID = 1L;
	static final long HOTSPOT_TIMEOUT = TimeUnit.MINUTES.toMillis(6);
	static final long WIFIBUSYTIMEOUT = TimeUnit.SECONDS.toMillis(45);
	static final long PING_TIMEOUT = TimeUnit.MINUTES.toMillis(3); // mset faster than system default? 
	static final long RESCAN = TimeUnit.SECONDS.toMillis(55);
	static final int autoSwitchCountMax = 2;
	
	HashMap<String, String> pings = new HashMap<String, String>(); // keep user list when in AP mode
	Vector<String> users = new Vector<String>(); // arp connected
	Vector<String> hosts = new Vector<String>(); // arp connected

	static Vector<String> monitor = new Vector<String>();
	
	Thread pingThread = new pingThread();
	Thread monitorThread = new monitorThread();
	Thread watchdogThread = new watchdogThread();
	
	static volatile boolean wifiBusy = false;
	static long lastping = System.currentTimeMillis();
	static long lastscan = System.currentTimeMillis();
	static long apTimerStart = System.currentTimeMillis();
	static int autoSwitchCount = 0;
	static long pingThreadId;
	
	static String currentSSID = null;
	static String pingtime = null;
	static String pingseq = null;
	static String pingip = null;
	static String ethaddress = null;
	static String ipaddress = null;
	static String gateway = null;
	static String edev = Util.getEthernetDevice();
	static String monitorID = null; // watch for new connections in monitor thread
	
	// debugging meta 
	static int wifiConnected, wifiDisconnected, wifiFailed = 0;
	
	// view flags
	static boolean menushowconnections = false;
	static boolean devmenu = false;
	static boolean debug = false;
	static boolean verbose = false;
	
	// lookup system info
	final static String host = Util.getHostName();
	final int radios = Util.countRadios(); // TODO: make plug, unplug safe
	final String release = Util.linuxVersion() + " " + Util.linuxCodeName();
	final String nmcliversion = Util.nmcliVersion();
	final String sharkv = Util.sharkVersion();

	static String remoteclientMsg = null;
	
	// from file
	static String wdev = null;
	static boolean configured_wdev = false;
	static boolean autoswitch = true;
	static String logfilename = null;
	static int signalstrengthlowthreshold = 35;
	static boolean priorityoverstrength = true;
	static int monitorLines = 30;
	static String apName = host;
	
	public void init(ServletConfig config) throws ServletException {
		super.init(config);	
		configure(Util.importFile());
		
		printlog("==== ACCESS POINT MANAGER BUILD: "+VERSION+" ====");      
		printlog("init(): nmcli version: "+ Util.nmcliVersion());
		printlog("init(): radios found: " + radios);
		
		Util.killApplet(); // older versions
		if (radios == 0) {
			printlog("init(): no radios found, fatal..");
			Util.killJetty();
		}
		
		if (wdev == null) wdev = Util.lookupDevice(); // if not in settings file choose first radio found 
		else configured_wdev = true; // BLOCK FUTURE LOOKUPS if in file
		printlog("init(): wdev: " + wdev);
		if (apName.equals(host)) apName = WirelessConnection.getAccessPiontConnection(); 	
		printlog("init(): AP name: " + apName);
		WirelessConnection.checkConfiguredHotsopt(apName, wdev);
		WirelessConnection.disableAutoSwitchOpenHotspots();
		AccessPoint.lookupAccessPoints();
		WirelessConnection.lookupWirelessConnections();

		lookupCurrentSSID();
		pingThread.start();
		monitorThread.start();
		watchdogThread.start();
		new scanThread().start();
		new arpThread().start();

		if (sharkv != null) new sharkThread().start();
	}

	// TODO: port filter 
	// sudo tshark -i wlx1cbfce17a9fe -T fields -e eth.addr -e dhcp.option.hostname -e dhcp.option.requested_ip_address -f 'port 67' 
	class sharkThread extends Thread {
				
		@Override
		public void run() {	
			
			if (wdev == null) return;
			
			Process proc = null;
			BufferedReader procReader = null;
			try{	
				
				String shark = "tshark -i "+wdev+" -T fields -e eth.addr -e dhcp.option.hostname -e dhcp.option.requested_ip_address ";				
				proc = Runtime.getRuntime().exec(new String[]{"/bin/sh", "-c", shark	});
						
				String line = null;		
				procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
				while ((line = procReader.readLine()) != null) {
					if (line.contains("ff:ff") && WirelessConnection.isHotspotMode()) {
							
						String[] tok = line.split(",");
						if (tok.length == 2) {
						
						//	Util.printlog("SharkThread: " + line + " v: " + line.split(",").length);
							String r = tok[1].substring(17).trim();
						//	Util.printlog("SharkThread: " + r + " length: " + r.split(" ").length);

							if (!hosts.contains(r)) hosts.add(r);
							
						}					
					}
				}
			} catch (Exception e) {
				Util.printlog("SharkThread: error reading from proc: " + e.getLocalizedMessage());
			}			
		}
	}

	String getHostName(String ip) {
		
		for (int i = 0 ; i < hosts.size() ; i++) {
			
			if (hosts.get(i).contains(ip)) return hosts.get(i);
			
		}
		return null;
	}

	
	// nmcli monitor thread watching for state changes 
	class monitorThread extends Thread {
		@Override
		public void run() {	
			
			Thread.currentThread().setName("monitorThread");
	      	
	        Process proc = null;
			BufferedReader procReader = null;
			try{	
				String line = null;		
				proc = Runtime.getRuntime().exec(new String[]{"nmcli", "monitor"});
				procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
				while ((line = procReader.readLine()) != null) doMonitor(line.trim());		
			} catch (Exception e) {
				printlog("monitorThread: error reading from proc: " + e.getLocalizedMessage());
			}	
		}
	}


	//  [chief]: wlx307bc9cda4d2: unavailable

	void doMonitor(String line) {
		try {
			
			if (line.contains(edev)) edev = Util.getEthernetDevice();
			
			if (line.startsWith(wdev)) {
				
				if (line.contains("connecting")) return;
				
				monitor(line); 
			
				printlog("monitorThread(): ["+ monitorID + "]: " + line);

				if (line.contains("disconnected")) {
					
					currentSSID = null; 
					killPing();
					wifiDisconnected++;
					autoSwitchCount = 0;
					lookupCurrentSSID();
					wifiBusy = false;
				}
				
				if (line.contains("using connection")){
					monitorID = line.substring(line.indexOf("'")+1, line.lastIndexOf("'"));		
					printlog("monitorThread(): using id: "+ monitorID);	
					wifiBusy = true;
				}
				
				if (line.contains("connected") && !line.contains("disconnected")) {
				
					wifiConnected++;	
					printdebug("monitorThread(): connected: " + monitorID);
					autoSwitchCount = 0;
					lookupGateway();
					WirelessConnection.lookupWirelessConnections();
					lookupCurrentSSID();
					
					if (WirelessConnection.isHotspotMode()) {		
						remoteclientMsg = "starting access point mode: " + currentSSID;
						printdebug("monitorThread(): ap mode starting, ap size: " + AccessPoint.access.size());
						apTimerStart = System.currentTimeMillis();
					
					} else reset(); // lookup, re-start ping
					
					wifiBusy = false;
				}
			
				// example: KARREMASAURUS: connection profile changed
				if (line.contains("connection profile changed")) {
					final String id = line.substring(0, line.indexOf(":"));
					Util.testNewConnection(id);		
					printdebug("monitor thread: failed to connect, or was booted off: "+ id);
				}

				if (line.contains("connection failed")) {
					wifiFailed++;
					remoteclientMsg = wifiFailed + " connection failed: "+ monitorID;
					printlog("monitorThread(): failed connection id: "+ monitorID);
					Util.testNewConnection(monitorID); // delete if never seen 	
				} 
			}
		} catch (Exception e) {
			printlog("monitorThread(): "+ e.getLocalizedMessage());
			e.printStackTrace();
		}	
	}
		
	void rescan() throws Exception {
	
		if (verbose) printdebug("rescan(): called seconds ago: " + (System.currentTimeMillis() - lastscan)/1000);
	
		if (WirelessConnection.isHotspotMode()) Util.disconnect();
		if (WirelessConnection.isHotspotMode()) 
			throw new Exception("can not scan in hotspot mode");
		
		if (((System.currentTimeMillis() - lastscan)) < TimeUnit.MINUTES.toMillis(1)) {
			if (verbose) printdebug("rescan(): called too soon: " + (System.currentTimeMillis() - lastscan)/1000);
			AccessPoint.lookupAccessPoints();
			return;
		}	
		
		try {			
			String[] cmd = new String[]{"/bin/sh", "-c", "nmcli d wifi rescan"}; 
			Runtime.getRuntime().exec(cmd);
		} catch (Exception e) {e.printStackTrace();}
		

		AccessPoint.lookupAccessPoints();
		lastscan = System.currentTimeMillis();
	}
	
	void killPing(){
		
		printdebug("pkill ping called");
		try {
			String[] cmd = new String[]{"/bin/sh", "-c", "pkill ping"};
			Runtime.getRuntime().exec(cmd);
		} catch (Exception e) {e.printStackTrace();}
	
		// kills thread while loop		
		pingThreadId = 0;
	}
	
	void configure(HashMap<String, String> settings){
		
		printdebug(settings.toString());

//		if (settings.containsKey("password")) hotspotPassword = settings.get("password");
		if (settings.containsKey("ssid")) apName = settings.get("ssid");
		if (settings.containsKey("wdev")) wdev = settings.get("wdev");	
		if (settings.containsKey("logfile")) logfilename = settings.get("logfile");
					
		if (settings.containsKey("autoswitch")) {
			if(settings.get("autoswitch").equalsIgnoreCase("false")) autoswitch = false;
		}
		
		if (settings.containsKey("verbose")) {
			if (settings.get("verbose").equalsIgnoreCase("true")) verbose = true;
		}
		
		if (settings.containsKey("debug")){
			if (settings.get("debug").equalsIgnoreCase("true")) debug = true;
			else debug = false;
		}
		
//		if (settings.containsKey("priorityswitching")){
//			if (settings.get("priorityswitching").equalsIgnoreCase("true")) priorityswitching = true;
//			else priorityswitching = false;
//		}
		
//		if (settings.containsKey("showpasswords")){
//			if(settings.get("showpasswords").equalsIgnoreCase("true")) showpasswords = true;
//			else showpasswords = false;
//		}
		
//		if (settings.containsKey("priorityoverstrength")){
//			if(settings.get("priorityoverstrength").equalsIgnoreCase("true")) priorityoverstrength = true;
//			else priorityoverstrength = false;
//		}
		
		if (settings.containsKey("devmenu")){
			if(settings.get("devmenu").equalsIgnoreCase("true")) devmenu = true;
		}
		
		if(settings.containsKey("signalstrengthlowthreshold"))
			signalstrengthlowthreshold = Integer.parseInt(settings.get("signalstrengthlowthreshold"));

//		if(settings.containsKey("signalstrengthmindiff"))
//			signalstrengthmindiff = Integer.parseInt(settings.get("signalstrengthmindiff"));

	}
	
	public void sendHTML(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		String html = null;
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println(headstyle(false)); // TODO:  check if mobile, send mobile css 

		if (wifiBusy) {
			out.println(waitForwifiBusy(request.getServerName()));
			return;
		}
		
		try {	
			html = toHTML();		
		} catch (Exception e) {
			// e.printStackTrace();	
			printdebug("tohtml(): "+e.getLocalizedMessage());
			html = new Date().toString() + " <br>\n error: "+ e.toString();
			out.println(fastrefresh());
			out.println(html);
			out.println(footer());
			out.close();
			return;
		}
		
		// refresh faster if about to change
		if (WirelessConnection.isHotspotMode()) {
			final long d = HOTSPOT_TIMEOUT - (System.currentTimeMillis() - apTimerStart);
			if (d < TimeUnit.MINUTES.toMillis(1)) {
				out.println(fastrefresh());
				out.println(html);
				out.println(footer());
				out.close();
				return;
			}
		}
					
		if (currentSSID == null) {
			// changing 
			out.println(fastrefresh());
			out.println(html);
			out.println(footer());
			out.close();
			return;
		}
		
		out.println(slowrefresh());
		out.println(html);
		out.println(footer());
		out.close();
		
//		if (verbose) printdebug("sendPage(): normal exit from ip: " + request.getRemoteAddr());
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// TODO: Phone skin 
		// if(request.getHeader("User-Agent").indexOf("Mobile") != -1) printdebug("mobile skin please");
		
		if (request.getQueryString() == null) {
			sendHTML(request, response);  
			return;
		}
		
		final Vector<WirelessConnection> wireless = WirelessConnection.wireless;
		
		String action = null;
		try { action = request.getParameter("action");
		} catch (Exception e) {	printlog("doGet(): " + e.getLocalizedMessage()); }

		String router = null;
		try { router = request.getParameter("router");
		} catch (Exception e) { printlog("doGet(): " + e.getLocalizedMessage()); }

		String password = null;
		try { password = request.getParameter("password");
		} catch (Exception e) { printlog("doGet(): " + e.getLocalizedMessage()); }

		if (action == null) {
			printdebug("null action.. ");
			sendHTML(request, response);  
			return;	
		}
		
//		if (verbose) printlog("doGet(): " + request.getRemoteAddr() + " " + request.getQueryString());
		
		if (debug && !action.equals("monitor") && !action.equals("xmlinfo") && !action.equals("status")) 
			printdebug("doGet(): " + request.getRemoteAddr() + " " + action);
		
		// don't block here  
		if (router != null && password != null && action.equals("create")) {
			printlog("doGet(): create ssid " + router + " with password ");
			wifiBusy = true;
			lastping = System.currentTimeMillis();
			apTimerStart = System.currentTimeMillis();
			response.sendRedirect("/");
			final String r = router;
			final String p = password;
			new Thread(new Runnable() {
				@Override
				public void run() {
					Util.createHotspotConnection(r, p, wdev);
					wifiBusy = false;
				}
			}).start();
			return;
		}
		

		if (action.equals("clear")){
			lastping = System.currentTimeMillis();
			apTimerStart = System.currentTimeMillis();
			wifiConnected = 0;
			wifiDisconnected = 0;
			wifiFailed = 0;
			lookupCurrentSSID();
			WirelessConnection.lookupWirelessConnections();
			response.sendRedirect("/");
			return;
		}
		
		if (router != null && password != null) {  
			printlog("doGet(): changeWIFI action? router: " + router + " and password");	
			wifiBusy = true;
			lastping = System.currentTimeMillis();
			apTimerStart = System.currentTimeMillis();
			testChangeWIFI(router, password);
			changeWIFI(router, password);
			response.sendRedirect("/");
			return;
		}
		
		if (action.equals("connect")){
			sendLogin(request, response, router);
			return;
		}
		
		if (action.equals("config")){
			sendConfig(request, response);
			return;
		}

		if (action.equals("create")){
			sendCreate(request, response);
			return;
		}

		if (action.equals("priup") && router != null) {
			printdebug("up: "+router);
			for (int i = 0 ; i < wireless.size() ; i++) {
				if (wireless.get(i).id.equals(router)) {
					Util.setPriority(router, wireless.get(i).priority+1);
					break;
				}		
			}
		}
		
		if (action.equals("pridw") && router != null) {
			printdebug("dw: "+router);
			for (int i = 0 ; i < wireless.size() ; i++) {
				if (wireless.get(i).id.equals(router)) {
					Util.setPriority(router, wireless.get(i).priority-1);
					break;
				}		
			}
		}
	
		if (action.equals("monitor")) {	
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			out.println(headstyle(false));
			out.println("<html><head></head><body>\n");			
			out.println("disconnected: " + wifiDisconnected + " connected: " + wifiConnected + " connecting failed: "+ wifiFailed + "\n");		
			out.println("<a href=\"?action=clear\">clear</a><br><br>\n");		
			for ( int i = 0 ; i < monitor.size() ; i++ )
				 out.println(monitor.get(i) + "<br>\n");
			
			out.println("\n\n <br><br><a href=\"?action=showconoff\">return to main screen</a>");
			out.println("\n\n <br><a href=\"?action=monitor\">refresh</a>");
			out.println("</body></html>\n");
			return;
		} 
		
		if (action.equals("status")){
			PrintWriter out = response.getWriter();
			out.println(statusXML());
			out.close();
			return;
		}
		
		// JS xmlhttp request
		if (action.equals("wifiready")) {
			if (verbose) printdebug("js xmlrpc wifiBusy "+String.valueOf(wifiBusy));
			PrintWriter out = response.getWriter();
			if (wifiBusy) out.print("");
			else {
				try { Thread.sleep(1000); } catch (InterruptedException e) {}
				out.print("ok");
			}
			out.close();
			return;
		}

		if (action.equals("xmlinfo")) {
			PrintWriter out = response.getWriter();
			out.print(infoToXML());
			return;
		}
		
		if (action.equals("up")) {
			lastping = System.currentTimeMillis();
			apTimerStart = System.currentTimeMillis();
			wifiBusy = true;
			final String r = router;
			new Thread(new Runnable() {
				@Override
				public void run() {
					testChangeWIFI(r);
					changeWIFI(r);
					wifiBusy = false;			
				}
			}).start();	
		}
		
		if (action.equals("open")) {
			wifiBusy = true;
			changeWIFI(router, null); // open router, no password 
		}
		
		if (action.equals("scan")) {
			wifiBusy = true;
			scanReset();
			wifiBusy = false;
		}
		
		if (action.equals("ap")) {
			wifiBusy = true;
			apTimerStart = System.currentTimeMillis();
			changeWIFI(apName); 
		}

		if (action.equals("down")) {
			currentSSID = null;
			response.sendRedirect("/"); // send user back, remove url args

			Util.disconnect();
			lookupCurrentSSID();
			lastping = System.currentTimeMillis();
			apTimerStart = System.currentTimeMillis();
			return;
		}
		
		if (action.equals("quickscan")) {
			wifiBusy = true;

			new Thread(new Runnable() {
				@Override
				public void run() {	
					try {
						rescan();
						wifiBusy = false;
					} catch (Exception e) {
						printdebug("doGet(): quick scan: "+ e.getLocalizedMessage());
					}
				}
			}).start();
		}
		
		response.sendRedirect("/"); // send user back, remove url args
		
		// save from stuck gui on wifi busy
		if (action.equals("break")) wifiBusy = false;
		if (action.equals("debugon")) debug = true; 
		if (action.equals("debugoff")) debug = false; 
		if (action.equals("devmenu")) devmenu=true; 
		if (action.equals("devmenuoff")) devmenu=false; 
		if (action.equals("showconoff")) menushowconnections=false; 
		if (action.equals("showcon")) menushowconnections =true; 
		if (action.equals("deletecon")) Util.connectionDelete(router);
		if (action.equals("reboot")) Util.reboot(); 
		if (action.equals("kill")) Util.killJetty();
	}
	
	void sendCreate(HttpServletRequest request, HttpServletResponse response) throws IOException{
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		boolean mobile = false;
//		if(request.getHeader("User-Agent").indexOf("Mobile") != -1) mobile = true;
		out.println(headstyle(mobile));
		out.println(passwordJavaScript());
		out.println("</head><body>\n");
		out.println("<form name='theform' method=\"post\"><table>"
				+ "<tr><td>SSID: </td><td><input type='hidden' name='action' value='create'><input type='text' name='router'></td><td></td></tr>\n"
				+ "<tr><td>Password: </td><td><input id=\"password\" type=\"password\" name=\"password\"></td> \n"
				+ "<td><a href='javascript: document.theform.submit()'>Create</a></td></tr> \n"
				+ "<tr><td colspan='3'><input type='checkbox' id='toggle' value='0' onchange='togglePassword(this);'></imput> show password </td>\n"
				+ "</table></form> \n");
		out.println("\n</body>");
	}

	void sendConfig(HttpServletRequest request, HttpServletResponse response) throws IOException{
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		boolean mobile = false;
//		if(request.getHeader("User-Agent").indexOf("Mobile") != -1) mobile = true;
		out.println(headstyle(mobile));
		out.println(passwordJavaScript());
		out.println("</head><body>\n");
		out.println("<form name='theform' method=\"post\"><table>"
				+ "<tr><td>Network Name / SSID:&nbsp;&nbsp;</td><td> <input type=\"text\" name=\"router\"></td><td></td></tr>\n"
				+ "<tr><td>password </td><td><input id=\"password\" type=\"password\" name=\"password\"></td>"
				+ "<td> &nbsp; <a href='javascript: document.theform.submit()'>CONNECT</a></td></tr>"
				+ "<tr><td colspan=\"3\"><input type='checkbox' id='toggle' value='0' onchange='togglePassword(this);'></imput> show password <br>\n"
				+ "</table></form>");
		out.println("\n</body>");
	}

	void sendLogin(HttpServletRequest request, HttpServletResponse response, String ssid) throws IOException{
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		boolean mobile = false;
//		if(request.getHeader("User-Agent").indexOf("Mobile") != -1) mobile = true;
		out.println(headstyle(mobile));
		out.println(passwordJavaScript());
		out.println("</head><body>\n");
		out.println("<form name='theform' method=\"post\"><table>"
				+ "<tr><td>Network Name / SSID:&nbsp;&nbsp;</td><td>" + ssid + "</tr><tr>"
				+ "<tr><td>Password:<td><input id=\"password\" type=\"password\" name=\"password\">"
				+ "<tr><td><input type='checkbox' id='toggle' value='0' onchange='togglePassword(this);'></imput> show password <br>\n"
				+ "<td><a href='javascript: document.theform.submit()'>CONNECT</a></td></tr> \n"
				+ "</table></form>");
		out.println("\n</body>");
		out.close();
	}

	String passwordJavaScript() {
		return "\n <script type=\"text/javascript\"> \n"
				+ "function togglePassword(el){  \n"
				+ "\tvar checked = el.checked; \n "
				+ "\tif(checked){ document.getElementById(\"password\").type = 'text'; document.getElementById(\"password\").textContent=\"Hide\";}\n"
				+ "\telse{ document.getElementById(\"password\").type = 'password';    document.getElementById(\"password\").textContent=\"Show\";}\n"
				+ "} \n</script>\n";
	}

	static String gap() { return "<table><tr><td style=\"height: 1px\"></td></tr></table>\n"; }

	// render main info page
	String toHTML() throws Exception {

		Vector<WirelessConnection> wireless = WirelessConnection.wireless;
		StringBuffer html = new StringBuffer();
		
		if (verbose) {
			html.append("disconnected: " + wifiDisconnected + " connected: " + wifiConnected + " connecting failed: "+ wifiFailed );		
			html.append("  <a href=\"?action=clear\">clear</a><br><br>\n");			
		}

		if (radios > 1 && !configured_wdev)
			html.append("<br><b>Multiple wifi devices: </b> check settings: " + wdev + "<br> \n");
	
		if (currentSSID == null) html.append("Not Connected<br> \n");
		else {
			if (WirelessConnection.isHotspotMode()) {
				html.append("Access Point SSID: <b>"+currentSSID+"</b><br> \n");
				if (Util.isHidden(currentSSID)) {
					html.append("&nbsp;&nbsp;&nbsp;&nbsp;<b>waring hidden SSID</b> <br>");
					//;&nbsp;&nbsp; <a href=\"kkkk\">(make public)</a><br> \n");
				}
			}
			else {
				
				html.append("Connected to Network: <b>" + currentSSID + " </b> &nbsp;");
				final int x = AccessPoint.indexOf(currentSSID);
				if (x != -1) html.append("<span style='font-size: 12px'>"+ AccessPoint.access.get(x).signalstrength + "%</span>");
				html.append("<br> \n");
					
			}
		}
		
		if (ethaddress != null) html.append("ETH address: <b>" + ethaddress + " </b><br> \n");
		if (ipaddress != null) html.append("WIFI address: <b>" + ipaddress + " </b><br> \n");
		if (gateway != null && debug) html.append("Gateway: <b> " + gateway + "</b><br> \n");
		if (host != null && debug) html.append("Host Name: <b>" + host + " </b><br> \n");
		if (configured_wdev) html.append("Radio: <b>" + wdev + "</b> (" + radios +")<br> \n");
		if (autoswitch && devmenu && verbose) html.append("Auto Switch: <b>enabled</b><br> \n");
		if (autoswitch && WirelessConnection.isHotspotMode()) {
			final long d = HOTSPOT_TIMEOUT - (System.currentTimeMillis() - apTimerStart);
			html.append("Auto Switch in: <b>"+d/1000+" </b>seconds <br> \n");
			if (d < 0) html.append("<b>Auto Switch Overdue</b><br> \n");
		}
		
		if (configured_wdev || verbose)
			html.append("WIFI Device: <b>" + wdev + "</b><br> \n");

		html.append("<br> \n");
		html.append(AccessPoint.getKnownHTML(currentSSID));
		html.append(AccessPoint.toHTML(currentSSID));
		if (menushowconnections) html.append(connectionHTML());

		if (users != null) {
			if (WirelessConnection.isHotspotMode() && users.size() > 0) {
				html.append("<br>");
				html.append("Connected to Hotspot:<br>"+gap());
				for (int i = 0 ; i < users.size() ; i++) { 
									
					String p = pings.get(users.get(i));
					if (p == null) p = "";	
					else p = "&nbsp;(" + p + ")&nbsp;";
					
					String h = getHostName(users.get(i));
					if (h == null) h = users.get(i);

					// h = h.replace(users.get(i), "").trim();
					
					html.append("&nbsp;"+ h + " " + p);	
					html.append("<br> \n");
				}
				html.append("<br>"+gap());
			}
		}
		
		html.append("\n<br>Commands:<br>"+gap()+"\n"); 
	 
		if (menushowconnections) 
			html.append("&nbsp;&nbsp;<a href=\"?action=showconoff\">Hide wireless connections</a><br> \n "); 
		else
			html.append("&nbsp;&nbsp;<a href=\"?action=showcon\">Show wireless connections</a> ("+ wireless.size()+")<br>\n"); 
		
		if (devmenu) {
			html.append("&nbsp;&nbsp;<a href=\"?action=config\">Connect to hidden router</a> ("+ AccessPoint.hidden +")<br>\n");	
			html.append("&nbsp; <a href=\"?action=monitor\">View monitor log</a><br>\n");
		}

		if (devmenu && !WirelessConnection.isHotspotMode())
			html.append("&nbsp;&nbsp;<a href=\"?action=quickscan\">Scan Now</a><br>\n");
		
		if (WirelessConnection.isHotspotMode())
			html.append("&nbsp;&nbsp;<a href=\"?action=scan\">Disconnect temporarily and scan wifi</a><br>\n");

		if (devmenu) {

			if (currentSSID != null) 
				html.append("&nbsp;&nbsp;<a href=\"?action=down\">Disconnect from: </a><b>"+ currentSSID +"</b><br>\n");

			if ( !WirelessConnection.isHotspotMode()) html.append("&nbsp;&nbsp;<a href=\"?action=ap\">Start access point: </a><b>"+apName+"</b><br>\n");
			
			html.append("&nbsp;&nbsp;<a href=\"?action=create\">Create new hotspot with password</a><br>\n");

			if (debug) html.append("&nbsp;&nbsp;<a href=\"?action=kill\">Shutdown Daemon</a><br>\n");
			if (debug) html.append("&nbsp;&nbsp;<a href=\"?action=reboot\">Reboot</a><br>\n");
			
			if (!debug) html.append("&nbsp; <a href=\"?action=debugon\">Turn debug on</a><br>\n");
			else html.append("&nbsp;&nbsp;<a href=\"?action=debugoff\">Turn debug off</a><br>\n");
		}

		if (!devmenu) html.append("&nbsp;&nbsp;<a href=\"?action=devmenu\">More commands</a>\n");
		else html.append("&nbsp;&nbsp;<a href=\"?action=devmenuoff\">Less commands</a>\n");

		if (debug && devmenu) {		
			html.append("<br>");
			html.append("<br><span style='font-size: 12px'><i><a href=\"https://bitbucket.org/bradz/access-point-manager\">Access Point Manager "+VERSION+"</a></i></span>\n");
			html.append("<br><span style='font-size: 12px'><i>"+nmcliversion.replace(",", "")+"</i></span>\n");
			html.append("<br><span style='font-size: 12px'><i>"+release +"</i></span>\n");
			if (sharkv != null)
				html.append("<br><span style='font-size: 12px'><i>"+sharkv +"</i></span>\n");

		}
		
		return html.toString();
	}

	
	String connectionHTML() {
		Vector<WirelessConnection> wireless = WirelessConnection.wireless;	
		StringBuffer html = new StringBuffer();
		try {
			html.append("\n<br>\n"); 
			html.append("Wireless Connections:<br>"+gap());
			for (int i = 0; i < wireless.size() ; i++) {
		
				String pri = "&nbsp;<a href=\"?action=priup&amp;router=" + wireless.get(i).id+ "\">&nbsp;&uarr;&nbsp;</a>"+ wireless.get(i).priority +
						"&nbsp;<a href=\"?action=pridw&amp;router=" + wireless.get(i).id+ "\">&darr;</a>&nbsp;";
				
				if (wireless.get(i).priority == 0) pri = pri.replace("&darr;", "&nbsp;");
				
				String link = "&nbsp;<a href=\"?action=up&amp;router=" + wireless.get(i).id+ "\">" + wireless.get(i).id + "</a>";
				if (wireless.get(i).active) link = "&nbsp;" + wireless.get(i).id.replace(" ", "&nbsp;") + "</a>";
				
				// ap, hostop, dups, can be different 
				if (!wireless.get(i).id.equals(wireless.get(i).ssid)) link += "&nbsp;(ssid: " + wireless.get(i).ssid + ")&nbsp;";
				if (wireless.get(i).isHotspot) link += "&nbsp;(ap)&nbsp;";
				if (wireless.get(i).active) link += "&nbsp;(active)";
	//			if (wireless.get(i).autoconnect) link += "&nbsp;(auto)";
	
				if (Util.isHidden(wireless.get(i).ssid)) link += "&nbsp; (hidden)"; 
				
	//			try {
	//				if (wireless.get(i).device != null) link += "&nbsp;("+ wireless.get(i).device + ")";
	//			} catch (Exception e) {}
				
				html.append(pri + link);
				if (!wireless.get(i).id.equals(apName) && !wireless.get(i).active)
					html.append("&nbsp;&nbsp;<a href=\"?action=deletecon&amp;router="
						+ wireless.get(i).id + "\">delete</a>");
	
				// TODO: line by line, make this a table 
				html.append("<br> \n");
			}
		} catch (Exception e) {}
		return html.toString();

	}
	
	class pingThread extends Thread {
		@Override
		public void run() {
			
			Thread.currentThread().setName("pingThread");
	      
			if (gateway == null) lookupGateway();
			if (gateway == null) {
				printlog("pingThread: no gateway, skipping..");
				return;
			}
			
			double avg = 0;
			int dropped = 0;
			final String thisgateway = gateway;
			final String connection = currentSSID;
			printlog("pingThread: starting thread, gateway: "+thisgateway+", ssid: "+currentSSID);
			Process proc = null;
			BufferedReader procReader = null;
			
			try {	
				
				proc = Runtime.getRuntime().exec(new String[]{"ping", "-I"+wdev, thisgateway});
				procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));

				lastping = System.currentTimeMillis();
				pingThreadId = lastping;
				long currentPingThreadId = pingThreadId;

				String line;
				while (((line = procReader.readLine()) != null) && (currentPingThreadId == pingThreadId )){
					try {
						
						//	From 10.42.0.1 icmp_seq=1306 Destination Host Unreachable
						if (line.contains("Unreachable")) {
							printdebug("pingThread():" + line);
							printdebug(thisgateway + " Unreachable count: " + dropped);
							dropped++;
							if (dropped > 5) return; // todo: should call autoswitch here, faster time out!
						}
						
						// don't parse ex: 539 packets transmitted, 539 received, 0% packet loss, time 550736ms
						if(line.contains("time") && line.contains("ms") && !line.contains("packets") && !line.contains("avg")) {	
							
							// example: 64 bytes from 192.168.1.1: icmp_seq=1557 ttl=64 time=0.263 ms
							
							if (line.contains("from") && line.contains(": icmp"))
								pingip = line.substring(line.indexOf("from ")+5, line.indexOf(": icmp")).trim();
							
							if (line.contains("icmp_seq=") && line.contains("ttl")) {
								pingseq = line.substring(line.indexOf("icmp_seq="), line.indexOf("ttl")).trim();
//								pingseq = pingseq.replaceAll("icmp_seq=", "");
							}
							
							if (line.contains("time=") && line.contains(" ms"))
								pingtime = line.substring(line.indexOf("time=")+5, line.indexOf(" ms")).trim();
							
							lastping = System.currentTimeMillis();
							if (pingseq != null && pingtime != null) {
								
								avg += Double.parseDouble(pingtime);
								if (Integer.parseInt(pingseq.split("=")[1])% 10==0) { 
									pings.put(connection, Util.formatFloat(avg/10, 2));
									avg = 0;
								}
							}
						}
					} catch (Exception e) {
						printdebug("pingThread: input was: "+line);
						printlog("pingThread: parse error: " + e.getLocalizedMessage());
						// don't break loop on parse error 
					}
				} // end while 
			} catch (Exception e) {
				printlog("pingThread: error reading from proc: " + e.getLocalizedMessage());
			}
			
			try {
				procReader.close();
			} catch (IOException e) {
				printlog("pingThread: error reading from proc: " + e.getLocalizedMessage());
			}
			
			printdebug("pingThread: gateway: "+thisgateway+" exiting");
		}
	}
	
	// switches to AP mode if ping timeout and not currently already in AP mode 
	class watchdogThread extends Thread {
		@Override
		public void run() {
			
			Thread.currentThread().setName("watchdogThread");
		      
			while(true) {
				try{

					Thread.sleep(5000);
					if ((System.currentTimeMillis() - lastping  > PING_TIMEOUT) && !WirelessConnection.isHotspotMode()) {
						
						printlog("watchdogthread(): last gateway ping was seconds ago: " + (System.currentTimeMillis() - lastping)/1000);	
						
						// lookupCurrentSSID();
						if (currentSSID == null) {
							printlog("not connected, starting AP..");
							lastping = System.currentTimeMillis();
							apTimerStart = System.currentTimeMillis();
							changeWIFI(apName);
						}
					}
				} catch (Exception e) {
					printlog("watchdogThread(): " + e.getLocalizedMessage());
				}
			}
		}
	}

	// scan for connected while in ap mode 
	class arpThread extends Thread {
		@Override
		public void run() {		
			
			Thread.currentThread().setName("arpThread");
			
			while(true) {
				try {	
					Thread.sleep(RESCAN / 3);	
					if (WirelessConnection.isHotspotMode()) {			
						users = Util.lookupARP(wdev);			
						if (users.size() > 0) {
							printdebug("arp(): connected: "+users);
							pingConnected();
						}
					} 
					
					else if (users != null) users.clear(); // over kill, be sure
					
				} catch (Exception e) {
					printlog("arpThread: error looping: " + e.getLocalizedMessage());
				}
			}
		}
	}	

	void pingConnected(){
		printdebug("pingConnected(): users = " + users.size());

		for( int i = 0; i < users.size() ; i++) {
			
			String fu = users.get(i);
			String p = Util.ping(fu, wdev);
			if (p != null) {
				
				printdebug("ping("+ fu + ") latency = " + p);
				pings.put(fu, p);
				
			} else {
				
				printdebug("ping("+ fu + ") failed: arp delete:" +fu);
				pings.remove(fu);
				try {
					Runtime.getRuntime().exec(new String[]{"arp", "-d", fu});
				} catch (IOException e) {}
			}
		}
	}
	
	
	// switches to another router if low signal
	class scanThread extends Thread {
		@Override
		public void run() {
			
			Thread.currentThread().setName("scanThread");
			if ( ! Util.wifiRadioReady(wdev)) Util.wifiEnable();
			
			// if starting up, scan first 
			/*
			if (WirelessConnection.isHotspotMode()) {
				Util.disconnect();
				try {					
					rescan();
				} catch (Exception e) {
					printdebug("scanThread(): "+ e.getLocalizedMessage());
				}
				apTimerStart = System.currentTimeMillis();
				changeWIFI(apName);
			}*/
			
			if ( ! WirelessConnection.isHotspotMode() || currentSSID == null) {
				try {					
					rescan();
				} catch (Exception e) {
					printdebug("scanThread(): "+ e.getLocalizedMessage());
				}
			}
					
			while (true) {
				try{

					Thread.sleep(RESCAN/3);
					if (wifiBusy) continue;
					if (currentSSID == null) {
						lookupCurrentSSID();
						continue;
					}
					
					if ( ! WirelessConnection.isHotspotMode()) {	
						if ((System.currentTimeMillis() - lastscan) > RESCAN) {	
							rescan(); 
							autoSwitch();
						}
					}
					
					// scan doesn't work in AP mode 	
					if (WirelessConnection.isHotspotMode()) { 
						if (HOTSPOT_TIMEOUT < (System.currentTimeMillis() - apTimerStart)) {
						
							if (WirelessConnection.countNonAPConnections() == 0) {
								
								printlog("scanThread(): NOTHING TO CHANGE To, reset timer");
								Util.disconnect();
								rescan();
								apTimerStart = System.currentTimeMillis();
								changeWIFI(apName);
								continue;	
							}
									
							if (users.size() > 0) {
								printlog("scanThread(): users connected to AP, reset timer");
								apTimerStart = System.currentTimeMillis();
								continue;
							}
							
							printlog("scanThread(): beak out of ap mode, disconnect..");
							Util.disconnect();
							if ( ! Util.wifiRadioReady(wdev)) Util.wifiEnable();

							rescan();
							testBreakout(); // wait 30 sec 
							// let system try first 
									
						}			
					}	
				} catch (Exception e) {
					printlog("scanThread(): " + e.getLocalizedMessage());
				}
			}
		}
	} 
	
	// wait for network manager to try, if not try autoswitch
	void testBreakout(){
		new Thread(new Runnable() {
			@Override
			public void run() {
				
				try { Thread.sleep(TimeUnit.SECONDS.toMillis(30)); } catch (Exception e) {e.printStackTrace();}
					
				if ( ! WirelessConnection.isHotspotMode()) {
					if (currentSSID != null) {
						printlog("testBreakout(): out of hot spot mode, ssid: "+ currentSSID);
						return;
					}
				}
				
				// TODO: loop wait
				//if (wifiBusy) {
			 	//	printlog("testBreakout(): wifibusy, skipping.. ");
				//	return;
				//}
				
		 		printlog("testBreakout(): still not out of ap mode.. ");
		 		
		 		WirelessConnection.lookupWirelessConnections();
				lookupCurrentSSID();
				if (currentSSID == null) {
					
					printlog("testBreakout(): null ssid, try strongest..");
					Vector<AccessPoint> access = AccessPoint.access;
					String tryme = access.get(getStrongestSSID()).ssid;
					changeWIFI(tryme);
					
				}
			}
		}).start();
	}
	
	// check signal strength and most used connection
	void autoSwitch() {

		if ( ! autoswitch) return; // disabled in settings? 
		
		if (WirelessConnection.isHotspotMode()) {
			printdebug("autoSwitch():  ap mode..wtf??");	
			return;
		}

		if (wifiBusy) {
			printdebug("autoSwitch(): wifibusy, skipping.. ");
			return;
		}
		
		Vector<AccessPoint> access = AccessPoint.access;
		Vector<WirelessConnection> wireless = WirelessConnection.wireless;
			
		if (access.size() == 0) {
			printdebug("autoSwitch(): null ap's, rescan.. ");
			Util.disconnect();
			try {
				rescan();
			} catch (Exception e) {
				if (verbose) printdebug("autoSwitch(): rescan: " +e.getLocalizedMessage());
//				e.printStackTrace();
			}
		}
		
		// getting too low ?
		final int strength = access.get(AccessPoint.indexOf(currentSSID)).signalstrength;
		if (strength > signalstrengthlowthreshold) {
			if (verbose) printdebug("autoSwitch(): signal = " + strength + " is good, not switching..");	
			return;
		} 

		if (WirelessConnection.countNonAPConnections() == 0) {
			printdebug("autoSwitch(): no known connections available, skipping.. ");		
			return;
		}
	
		if (access.size() <= 1) {
			printdebug("autoSwitch(): no known connections available, skipping.. "+ access.size());		
			return;
		}

		if (wireless.size() <= 1) {	// ap only 					
			printdebug("autoSwitch(): no connections available, skipping.. "+ wireless.size());			
			return;
		}
		
		if (verbose) printdebug("autoSwitch(): ap size: " +access.size() + " connection: " + wireless.size());
		
		// cleared by monitor thread after new connection made 
		if (autoSwitchCount > 0) printdebug("autoSwitch(): auto count; " + autoSwitchCount);
	
		int best = -1;
		try {
			best = getStrongestSSID();
		} catch (Exception e) {
			printdebug("autoswitch(): error getting strongest ssid ");
			return;
		}
		
		if (best == -1) {
			printdebug("autoswitch(): error: getting best ssid");
			return;
		}
		
		try {
			if ( ! currentSSID.equals(access.get(best).ssid)) {
				
				autoSwitchCount++;
				if (autoSwitchCount > (autoSwitchCountMax+1)) {  

					printdebug("autoSwitch(): switching to stronger signal: " + access.get(best));		
					remoteclientMsg = "messageclients wifi switching to stronger signal "+ access.get(best);
					changeWIFI(access.get(best).ssid);  
					
				}	
			}
		} catch (Exception e) {
			e.printStackTrace();
			printdebug("autoswitch(): error: " + e.getLocalizedMessage());
			printdebug("autoswitch(): back to ap mode, fail to connect to: " + access.get(best));
			
			try { Thread.sleep(1000); } catch (InterruptedException e1) {}
			changeWIFI(apName);
		}	
	}
	
	// if all in range, choose by priority 
	int getStrongestSSID() { 

		int indexhighest = 0;
		Vector<AccessPoint> access = AccessPoint.access;
		
		// determine strongest signal in list, that is a known connection
		
		try {
			int highestvalue = 0;
			for (int i = 0; i < access.size(); i++) {	
				
				final int value = access.get(i).signalstrength;
				
				if (value > highestvalue && WirelessConnection.isKnownSSID(access.get(i).ssid)) { 
					indexhighest = i;
					highestvalue = value;
					printlog("getStrongestSSID(): high value: " + highestvalue + " " + access.get(i));		
				}
			}		
		} catch (Exception e) {
			printlog("getStrongestSSID(): " + e.getLocalizedMessage());
		}
		
		return indexhighest;
	}
	
//	boolean waitForWifi() {
//		long start = System.currentTimeMillis();
//		while (wifiBusy && System.currentTimeMillis()-start < WIFIBUSYTIMEOUT)
//			try { Thread.sleep(25); } catch (InterruptedException e) {e.printStackTrace(); }
//		if (!wifiBusy) return true;
//		else return false;
//	}
	

	// wait for network manager to try, try twice if fail
	void testChangeWIFI(final String target){
		new Thread(new Runnable() {
			@Override
			public void run() {
				
				try {Thread.sleep(TimeUnit.SECONDS.toMillis(30)); } catch (Exception e) {e.printStackTrace();}
						 		
				lookupCurrentSSID();
				if (currentSSID == null) {
					
					changeWIFI(target);
					
				} else {
				
					if ( ! currentSSID.equals(target))
						changeWIFI(target);
					
				}
			}
		}).start();
	}
	
	void testChangeWIFI(final String target, final String pw){
		new Thread(new Runnable() {
			@Override
			public void run() {
				
				try {Thread.sleep(TimeUnit.SECONDS.toMillis(30)); } catch (Exception e) {e.printStackTrace();}
		 		
				lookupCurrentSSID();
				if (currentSSID == null) changeWIFI(target, pw);
				else if ( ! currentSSID.equals(target)) changeWIFI(target, pw);
					
			}
		}).start();
	}
	
	synchronized static void changeWIFI(final String ssid, final String password){
		
		if(wdev == null || ssid == null ) {
			printlog("changeWIFI("+ssid+", password): null args, abort..");
			return;
		}
			
		new Thread(){
			public void run() {
				
				if (password == null) printlog("changeWIFI("+ssid+", no password) called..");
				else printlog("changeWIFI("+ssid+", and password) called..");
				
				if (WirelessConnection.isHotspotMode()) {
					printlog("changeWIFI("+ssid+", password): ap mode, disconnecting first..");
					Util.disconnect();
					try{Thread.sleep(1500);} catch (InterruptedException e) {}
				}
						
				//	
				//  OVERKILL, let system re-conect 
				//
				//	wifiDisable();
				//	wifiEnable();
				//	try {Thread.sleep(3000);} catch (InterruptedException e) {}

				if ( !Util.wifiRadioReady(wdev)) Util.wifiEnable();

				try {

					String cmd = "nmcli dev wifi connect \""+ssid+"\"";
					if (password != null) cmd += " password \""+password +"\"";
					cmd += " ifname " + wdev; 
					
				//	if (verbose) printlog("changeWIFI("+ssid+", with password) exec: " + cmd);
					
					Process proc = Runtime.getRuntime().exec( new String[]{"/bin/sh", "-c", cmd});
					proc.waitFor();
					printlog("changeWIFI(ssid, password): [" + ssid + "] exit code: " + proc.exitValue());
                    // returns 0 even if password fails, connection file created will be deleted later 
					
				} catch (Exception e) {
					printlog("changeWIFI(ssid, password): Exception: " + e.getMessage());
				}
				
				wifiBusy = false;
			}
		}.start();
	}

	synchronized static void changeWIFI(final String ssid){ 

		if (ssid == null) return;
		
		new Thread(){
			public void run() {

				printlog("changeWIFI(" + ssid + ")");
				if (WirelessConnection.isHotspotMode()) {
					printlog("changeWIFI(" + ssid + "): ap modee, disconnect first..");
					Util.disconnect();
					try { Thread.sleep(1500); } catch (InterruptedException e) {}
				}
				
				if ( !Util.wifiRadioReady(wdev)) Util.wifiEnable();
				
				try {
 
					Process proc = Runtime.getRuntime().exec(new String[]{"/bin/sh", "-c", "nmcli c up id \"" + ssid + "\""});
					proc.waitFor();
					printlog("changeWIFI(): [" + ssid + "] exit code: " + proc.exitValue());			
					
				} catch (Exception e) {
					printlog("changeWIFI(" + ssid + "): exception: " + e.getMessage());
				}
			
				// break out early? monitor thread toggles bug flag also 
				wifiBusy = false;	
			}
		}.start();
	}

	synchronized void reset(){
		printdebug("reset(): restart ping thread.. ");
		lastping = System.currentTimeMillis();
		lookupCurrentSSID();
		lookupGateway();
		pingThread = new pingThread();
		pingThread.start();
	}

	// disconnect and scan, reconnect to previous connection (if any)
	synchronized void scanReset(){
		wifiBusy = true;
		final String lastconnection = currentSSID;
		Util.disconnect();
		try { Thread.sleep(3000); } catch (InterruptedException e) {}	
		AccessPoint.lookupAccessPoints();
		printdebug("getAccessPoints(): found: " + AccessPoint.access.size());
		if (lastconnection != null) changeWIFI(lastconnection);
		wifiBusy = false;
	}

	static void lookupGateway(){

		gateway = null;
		ipaddress = null;
		ethaddress = null;
	
		if (edev == null) edev = Util.getEthernetDevice(); // keep trying, never know if was just plugged in 
		if (wdev == null) wdev = Util.lookupDevice();	
		
		try {

			Process proc = Runtime.getRuntime().exec(new String[]{"/bin/sh", "-c", "ip route show"});
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			String line = null;

			while ((line = procReader.readLine()) != null) {

				String words[] = line.split(" +");

				if (line.startsWith("default via") && line.contains(wdev)) 
					if (Util.validIP(words[2])) 
						gateway = words[2];
				
				if (wdev != null) 
					if (line.contains("dev "+wdev) && words.length >= 9) 
						if (Util.validIP(words[8])) ipaddress = words[8];
						
				if (edev != null) 
					if (line.contains("dev "+edev) && words.length >= 11) 
						if (Util.validIP(words[8])) ethaddress = words[8];
			}
		} catch (Exception e) {
			printlog("lookupGateway(): exception: " + e.getLocalizedMessage());
			ipaddress = null;
			ethaddress = null;
			gateway = null;
		}
	}
	
	static void lookupCurrentSSID(){
/*
		printdebug("____lookupCurrentSSID() monitor:     "+ monitorID);
		printdebug("____lookupCurrentSSID() id:          "+WirelessConnection.getCurrentConnectionID());
		printdebug("____lookupCurrentSSID() ssid:        "+WirelessConnection.getCurrentSSID());
		printdebug("____lookupCurrentSSID() util:        "+Util.lookupCurrentSSID(wdev));

*/	
		// WirelessConnection.lookupWirelessConnections();	
		currentSSID = Util.lookupCurrentSSID(wdev);
		
		if (WirelessConnection.isHotspotMode( WirelessConnection.getCurrentSSID() )) {
			
			printdebug("lookupCurrentSSID() hotspot: "+WirelessConnection.getCurrentSSID());
			currentSSID = WirelessConnection.getCurrentSSID();
		}
		
		printdebug("lookupCurrentSSID(): " + currentSSID );
	}		

	static void printlog(String str) {
		monitor(str);
		if (verbose) str = "(v: " + VERSION + ") " + str;
		System.out.println(new Date().toString()+" "+str);
		Util.toFile(logfilename, str);
	}

	static void printdebug(String str) {
		if (!debug) return;
		if (verbose) str = "(v: " + VERSION + ") " + str;
		System.out.println(new Date().toString()+" "+str);
		Util.toFile(logfilename, str);
	}

	static void monitor(final String line) {	
		if (monitor.size() > 2)	if (monitor.lastElement().contains(line)) return;
		while(monitor.size() > monitorLines) monitor.remove(0);
		monitor.add(Util.getDateStamp() + "   " + line);
	}
	
	String getCSS(boolean mobile){
		StringBuffer buffer = new StringBuffer();
		buffer.append("\n<style type=\"text/css\"> \n");

//		if(mobile){
//			buffer.append("body, p, ol, ul { \nfont-family: verdana, arial, helvetica, sans-serif; font-size: 32px;} \n");
//		} else {
		buffer.append("body, p, ol, ul { \nfont-family: verdana, arial, helvetica, sans-serif; font-size: 16px;} \n");
//		}


		buffer.append("th, td { text-align: left; padding: 2px; }");
//		buffer.append("tr:nth-child(even){background-color: #f2f2f2}");
		buffer.append("th { background-color:  #80bfff; color: white; }");
//		buffer.append("table, th, td {border: 1px solid black;}");
		
		buffer.append("a { text-decoration: none; }\n</style> \n");
		return buffer.toString();
	}

	String headstyle(boolean mobile) {
		return "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n" +
				"<html><head>\n<title>Wifi Setup</title>\n" +
				"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" >" +
				"<meta http-equiv=\"Pragma\" content=\"no-cache\">\n" +
				"<meta http-equiv=\"Cache-Control\" Content=\"no-cache\">\n" +
				"<meta http-equiv=\"Expires\" content=\"-1\">\n" + getCSS(mobile);
	}

	String slowrefresh() {
		return "<meta http-equiv=\"refresh\" content=\"60\">\n" + "</head><body>\n";
	}
	
	String fastrefresh() {
		return "<meta http-equiv=\"refresh\" content=\"5\">\n" + "</head><body>\n";
	}
	
	String waitForwifiBusy(String addr) {
		return  "<script type=\"text/javascript\">\n" +
				"\n" +
				"var xmlhttp=null;\n" +
				"\n" +
				"function loaded() {\n" +
				"\tsetTimeout(\"sendWifiReadyCheck()\", 2000);\n" +
				"}\n" +
				"\n" +
				"function sendWifiReadyCheck() {\n" +
				"\t\n" +
				"\n" +
				"\tvar url = document.location.protocol +'//'+ \n" +
				"\t\tdocument.location.hostname + (location.port ? ':'+location.port: '') +\n" +
				"\t\tdocument.location.pathname;\n" +
				"\turl += \"?action=wifiready\";\n" +
				"\tconsole.log(url);\n" +
				"\topenxmlhttp(url, wifiReadyResponseReceived);\n" +
				"}\n" +
				"\n" +
				"function openxmlhttp(theurl, functionname) {\n" +
				"\t  if (window.XMLHttpRequest) {// code for all new browsers\n" +
				"\t    xmlhttp=new XMLHttpRequest();}\n" +
				"\t  else if (window.ActiveXObject) {// code for IE5 and IE6\n" +
				"\t    xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\"); \n" +
				"\t    theurl += \"?\" + new Date().getTime();\n" +
				"\t  }\n" +
				"\t  if (xmlhttp!=null) {\n" +
				"\t    xmlhttp.onreadystatechange=functionname; // event handler function call;\n" +
				"\t    xmlhttp.open(\"GET\",theurl,true);\n" +
				"\t    xmlhttp.send(null);\n" +
				"\t  }\n" +
				"\t  else {\n" +
				"\t    alert(\"Your browser does not support XMLHTTP.\");\n" +
				"\t  }\n" +
				"}\n" +
				"\n" +
				"function wifiReadyResponseReceived() {\n" +
				"\tif (xmlhttp.readyState==4) {// 4 = \"loaded\"\n" +
				"\t\tif (xmlhttp.status==200) {// 200 = OK\n" +
				"\t\t\tif (xmlhttp.responseText==\"ok\") {\n" +
				"\t\t\t\twindow.location.reload();\n" +
				"\t\t\t}\n" +
				"\t\t\telse { loaded(); }\n" +
				"\t\t}\n" +
				"\t}\n" +
				"}\n" +
				"\n" +
				"</script>\n" +
				"</head><body onload=\"loaded();\">\n" +
				"Please Wait...<br><br><a href=\"?action=break\"   > reset </a>\n" +
				"<br><br> <a href=\"?action=break\"> view log </a>\n";
	}
	
	String footer() {
		return "\n</body></html>";
	}


	String statusXML() {
		String xmlstr = "<?xml version='1.0' encoding='UTF-8'?>";

		try {
		
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("status");
			doc.appendChild(rootElement);
	
			Element rel = doc.createElement("os");
			rel.appendChild(doc.createTextNode(release));
			rootElement.appendChild(rel);
			
			Element h = doc.createElement("host");
			h.appendChild(doc.createTextNode(host));
			rootElement.appendChild(h);
			
			Element host = doc.createElement("booted");
			host.appendChild(doc.createTextNode(Util.formatTime(String.valueOf(System.currentTimeMillis()))));
			rootElement.appendChild(host);
			
			Element nmcli = doc.createElement("nmcli");
			nmcli.appendChild(doc.createTextNode(nmcliversion.replace("nmcli tool, version", "").trim()));
			rootElement.appendChild(nmcli);
			
			Element version = doc.createElement("version");
			version.appendChild(doc.createTextNode(String.valueOf(VERSION)));
			rootElement.appendChild(version);
			
			if (sharkv != null) {
				Element shark = doc.createElement("tshark");
				shark.appendChild(doc.createTextNode(String.valueOf(sharkv)));
				rootElement.appendChild(shark);
			}
			
			Element scan = doc.createElement("scan");			
			long scan_ms = System.currentTimeMillis() - lastscan;
			scan.appendChild(doc.createTextNode(Long.toString(scan_ms)));
			scan.setAttribute("units", "milliseconds");
			rootElement.appendChild(scan);
			
			// ping active
			if ( !WirelessConnection.isHotspotMode()) {
	
				Element d = doc.createElement("last");
				final String ms = Long.toString(System.currentTimeMillis() - lastping);
				d.appendChild(doc.createTextNode(ms));
				d.setAttribute("units", "milliseconds");
				rootElement.appendChild(d);
				
				Element lag = doc.createElement("lag");			
				lag.appendChild(doc.createTextNode(pingtime));
				lag.setAttribute("units", "milliseconds");
				rootElement.appendChild(lag);	
			
				try {
					Element seq = doc.createElement("seq");
					String s = pingseq.replace("icmp_seq=", "").trim();
					seq.appendChild(doc.createTextNode(s));
					rootElement.appendChild(seq);
				} catch (Exception e1) {}
			
			}
			 
			Element counters = doc.createElement("counters");
			rootElement.appendChild(counters);

			Element connected = doc.createElement("connected");			
			connected.appendChild(doc.createTextNode(Integer.toString(wifiConnected)));
			counters.appendChild(connected);

			Element dis = doc.createElement("disconnected");			
			dis.appendChild(doc.createTextNode(Integer.toString(wifiDisconnected)));
			counters.appendChild(dis);
			
			Element failed = doc.createElement("failed");			
			failed.appendChild(doc.createTextNode(Integer.toString(wifiFailed)));
			counters.appendChild(failed);
			
			if (users != null) {
				if (WirelessConnection.isHotspotMode() && users.size() > 0) {
					
					Element usr = doc.createElement("users");	
					usr.appendChild(doc.createTextNode(Integer.toString(users.size())));
					//	usr.setAttribute("size", String.valueOf(users.size()));
					rootElement.appendChild(usr);
					
				}		
			}
		
			
			Vector<WirelessConnection> wireless = WirelessConnection.wireless;
			if (wireless != null) {
				if (wireless.size() > 0) {
					
					Element networksknown = doc.createElement("wireless");
					rootElement.appendChild(networksknown);
	
					for (int i = 0; i < wireless.size(); i++) {
						
						Element connection = doc.createElement("connection");
										 
						Element ssid = doc.createElement("ssid");
						ssid.setTextContent(wireless.get(i).ssid);
					    connection.appendChild(ssid);
						
						Element id = doc.createElement("id");
						id.setTextContent(wireless.get(i).id);
						connection.appendChild(id);

						Element priority = doc.createElement("priority");
						priority.setTextContent(String.valueOf(wireless.get(i).priority));
						connection.appendChild(priority);
						
						// Element uuid = doc.createElement("uuid");
						// uuid.setTextContent(wireless.get(i).uuid);
						// connection.appendChild(uuid);
					
						try {
							Element last = doc.createElement("last");
							String time = String.valueOf(wireless.get(i).lastseen) +"000";
							last.setTextContent(Util.formatTime(time));
							connection.appendChild(last);
						} catch (Exception e) {}
						
						Element mode = doc.createElement("mode");
						if (wireless.get(i).isHotspot) {
							mode.setTextContent("hotspot");
							connection.appendChild(mode);
						}
						
						Element auto = doc.createElement("autoconnect");
						if (wireless.get(i).autoconnect) {		
							auto.setTextContent("enabled");
							connection.appendChild(auto);							
						}
						
						if (wireless.get(i).device != null){
							Element dev = doc.createElement("device");
							dev.setTextContent(wireless.get(i).device);	
							connection.appendChild(dev);
						}
				
						if (Util.isHidden(wireless.get(i).ssid)) {
							Element v = doc.createElement("visable");
							v.setTextContent("hidden");	
							connection.appendChild(v);
						}
						
						networksknown.appendChild(connection);
					}
				}
			}
			
			return xmlstr + Util.XMLtoString(doc);

		} catch (Exception e) {e.printStackTrace();}

		return xmlstr;
	}

	String infoToXML() {
		
		String xmlstr = "<?xml version='1.0' encoding='UTF-8'?>";

		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// root element 'networkinfo'
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("networkinfo");
			doc.appendChild(rootElement);
 
		
			// remote client message
			if (remoteclientMsg != null) {
				Element message = doc.createElement("message");
				message.appendChild(doc.createTextNode(remoteclientMsg));
				rootElement.appendChild(message);

				remoteclientMsg = null; // only show once
			}

			// gatewayaddres
			if(gateway != null) {
				Element gatewayaddress = doc.createElement("gatewayaddress");
				gatewayaddress.appendChild(doc.createTextNode(gateway));
				rootElement.appendChild(gatewayaddress);
			}
		
			// localaddress
			if (ipaddress != null) {
				Element localaddress = doc.createElement("localaddress");
				localaddress.appendChild(doc.createTextNode(ipaddress));
				rootElement.appendChild(localaddress);
			}
			
			// eth address
			if (ethaddress != null) {
				Element localaddress = doc.createElement("ethaddress");
				localaddress.appendChild(doc.createTextNode(ethaddress));
				rootElement.appendChild(localaddress);
			}
			
			// network current
			if (currentSSID != null) {
				Element networkcurrent = doc.createElement("networkcurrent");
				rootElement.appendChild(networkcurrent);
				Element name = doc.createElement("name");
				name.appendChild(doc.createTextNode(currentSSID));
				networkcurrent.appendChild(name);
			}

			// networks in range
			if (AccessPoint.access.size() > 0) {
				Element networksinrange = doc.createElement("networksinrange");
				rootElement.appendChild(networksinrange);

				for (int i = 0; i < AccessPoint.access.size(); i++) {
					
					if (AccessPoint.access.get(i).bssid == null) continue;
					
					Element network = doc.createElement("network");
					networksinrange.appendChild(network);

					Element name = doc.createElement("name");
					name.appendChild(doc.createTextNode(AccessPoint.access.get(i).ssid));
					network.appendChild(name);

					Element strength = doc.createElement("strength");
					strength.appendChild(doc.createTextNode(AccessPoint.access.get(i).signalstrength + "%"));
					network.appendChild(strength);
				}
			}
			
			return xmlstr + Util.XMLtoString(doc);

		} catch (Exception e) {e.printStackTrace();}

		return xmlstr;
	}
}


