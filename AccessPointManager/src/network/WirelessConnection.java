package network;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

public class WirelessConnection {

	
	public static Vector<WirelessConnection> wireless = new Vector<WirelessConnection>();

	public String id;
	public String uuid;
	public String ssid;
//	public String bssid;
	public boolean active;
	public boolean isHotspot;
	public boolean autoconnect;
	public int priority;
	public String device;
	public long lastseen;

	public WirelessConnection(final String line) {
		
		final String[] args = line.split(":");

		id = args[1]; 
		lastseen = Long.parseLong(args[2]);
		
		// null, not String("");
		device = args[3];
		if (device != null) {
			device = device.trim();
			if (device.length() == 0)
				device = null;
		}			 
		
		if (args[4].equals("no")) autoconnect = Boolean.FALSE;
		else autoconnect = Boolean.TRUE;
		
		priority = Integer.parseInt(args[5]); 
		
		if (args[6].equals("no")) active = Boolean.FALSE;
		else active = Boolean.TRUE;
		
		uuid = args[7];
		ssid = Util.getSSID(id);
		isHotspot = Util.getModeUUID(uuid);
		if (isHotspot) ssid = Util.getSSIDbyUUID(uuid);
	}
	
	public WirelessConnection(final WirelessConnection copy) {
		id = copy.id;
		uuid = copy.uuid;
	//	bssid = copy.bssid;
		active = copy.active;
		isHotspot = copy.isHotspot;
		autoconnect = copy.autoconnect;
		priority = copy.priority;
		device = copy.device;
		lastseen = copy.lastseen;
		
	}
	
	public String toString() {
		return id;
	}
	
	/*
	String getPassword(String hotspot) {	
		try { 
			String[] cmd = new String[]{"/bin/sh", "-c", "nmcli -t --show-secrets connection show " +  hotspot + " | grep psk:"};		
			Process proc = Runtime.getRuntime().exec(cmd);
			proc.waitFor();
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			String line = procReader.readLine();
			String[] args = line.split(":");
			return args[1];		
		} catch (Exception e) {e.printStackTrace();}
		return null;
	}
	
	*/
	
	public static String getAccessPiontConnection() {  
		int highest = -1;
		int index = -1;
		try {	
			for (int i = 0; i < wireless.size(); i++) {	
				if ( !wireless.get(i).isHotspot) continue;
				int value = wireless.get(i).priority;
				if (value > highest) {
					highest = value;
					index = i;
				}
			}	
		} catch (Exception e) {e.printStackTrace();}
		
		if (index == -1) return Util.getHostName();
		return wireless.get(index).id;
	}

	public static void disableAutoSwitchOpenHotspots() {
		for (int i = 0 ; i < wireless.size() ; i++) {			
			if (wireless.get(i).isHotspot) {
				Util.printlog("disableAutoSwitchOpenHotspots(): disable auto switch: "+ wireless.get(i).id);
				Util.setAutoConnect(wireless.get(i).id, false);		
			}
		}
		lookupWirelessConnections();
	}
	
	// check settings, create if needed
	public static boolean checkConfiguredHotsopt(String apName, String wdev) {
		
		if (apName == null || wdev == null) return false;
		
		Util.printlog("checkConfiguredHotsopt(): " + apName);
		
		if (wireless.size() == 0) lookupWirelessConnections();
		
		int ap = WirelessConnection.indexOfID(apName);
		if (ap == -1) {
			Util.printlog("checkConfiguredHotsopt(): doesn't exist, create..");
			Util.createAPConnection(apName, wdev);
			NetworkServlet.changeWIFI(apName);
			return true;
		}
		
		return false;
	}
	
	// todo: track timer 
	public synchronized static void lookupWirelessConnections() {	
		try { 
			wireless.clear();
			String[] cmd = new String[]{"/bin/sh", "-c", "nmcli -t -f type,name,timestamp,device,autoconnect,autoconnect-priority,active,uuid con | grep wireless"};
			Process proc = Runtime.getRuntime().exec(cmd);
			proc.waitFor();
			String line = null;
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			while ((line = procReader.readLine()) != null) {
				// String[] args = line.split(":");
				// if(args[0].contains("wireless") || args[0].contains("wifi")) // mncli version 1.10.14
					wireless.add(new WirelessConnection(line));
			}
		} catch (Exception e) {
			Util.printlog("lookupWirelessConnections(): exception: " + e.getLocalizedMessage());
		}
	
		sort(wireless);
		if (NetworkServlet.verbose) Util.printlog("lookupWirelessConnections(): size:" + wireless.size());
	}
	
	public static String getCurrentConnectionID() {
		for ( int i = 0 ; i < wireless.size() ; i++) {
			if( wireless.get(i).active ) 
				return wireless.get(i).id;	
		}
		return null;
	}
	
	
	public static String getCurrentSSID() {
		// Util.printlog("getCurrentSSID(): size:" + wireless.size());
		for ( int i = 0 ; i < wireless.size() ; i++) {
			if( wireless.get(i).active ) 
				return wireless.get(i).ssid;	
		}
		return null;
	}
	
	public static boolean isHotspotMode() {
		
//		Util.printlog("isHotspotMode(): size:" + wireless.size());
//		if (wireless.size() == 1) return true;
//		lookupWirelessConnections();
		
		for ( int i = 0 ; i < wireless.size() ; i++) {
			if( wireless.get(i).active ) {
				
				if (wireless.get(i).isHotspot) {
//					Util.printlog("__yes__isHotspotMode(): ssid:" + wireless.get(i).ssid + " id: " +  wireless.get(i).id );
					return true;
					
				}
			}
		}
		
		return false;
	}
	
	public static boolean isHotspotMode(String ssid) {
		
		for ( int i = 0 ; i < wireless.size() ; i++) {
			if( wireless.get(i).ssid.equals(ssid)) {
				if (wireless.get(i).isHotspot) {
					
					Util.printlog("isHotspotMode(ssid): hotspot:" + wireless.get(i).ssid + " id: " +  wireless.get(i).id );
					return true;
				}
			}
		}
		return false;
	}
	
	public static boolean isKnownSSID(String ssid) {
		if (ssid == null || wireless == null) return false;
		
		for (int i = 0 ; i < wireless.size() ; i++) {
			if (wireless.get(i).ssid.equals(ssid)) 
				return true;	
		}
		return false;
	}
	
	public static int indexOfSSID(String ssid) {
		if (ssid == null || wireless == null) return -1;

		for ( int i = 0 ; i < wireless.size() ; i++) {
			if( wireless.get(i).ssid.equals(ssid)) 
				return i;	
		}
		return -1;
	}
	
	static int indexOfID(String id) {
		for ( int i = 0 ; i < wireless.size() ; i++) {
			if( wireless.get(i).id.equals(id)) 
				return i;	
		}
		return -1;
	}
	
	
	// sort by priority, then time last accessed 
	public static void sort(Vector<WirelessConnection> list) {
		Collections.sort(list, new Comparator<WirelessConnection>() {
			public int compare(WirelessConnection f1, WirelessConnection f2) {	
				long result = f2.priority - f1.priority;
				if (result > 0) return 1;
				else if (result < 0) return -1;
				else { // equal priority, then time last seen
					if (f1.lastseen > f2.lastseen) return -1;
					else return 1;
				}
			}
		});
	}
	
	public static int countNonAPConnections() {
		int c = 0;
		if (wireless.size() == 0) lookupWirelessConnections();
		for ( int i = 0 ; i < wireless.size() ; i++) {
			if( ! wireless.get(i).isHotspot) 
				c++;
		}
		return c;
	}
}
